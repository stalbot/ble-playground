//  Copyright (c) 2014 Scott Talbot. All rights reserved.

@import Foundation;
@import CoreLocation;
@import IOBluetooth;

@interface STBeaconPeripheralDataHelper : NSObject
+ (NSDictionary *)peripheralDataWithProximityUUID:(NSUUID *)uuid major:(uint16_t)major minor:(uint16_t)minor measuredPower:(int8_t)power;
@end
@implementation STBeaconPeripheralDataHelper
+ (NSDictionary *)peripheralDataWithProximityUUID:(NSUUID *)uuid major:(uint16_t)major minor:(uint16_t)minor measuredPower:(int8_t)power {
    uint8_t advertisementBytes[21] = { 0 };

    [uuid getUUIDBytes:&advertisementBytes[0]];

    advertisementBytes[16] = major >> 8;
    advertisementBytes[17] = major & 0xff;

    advertisementBytes[18] = minor >> 8;
    advertisementBytes[19] = minor & 0xff;

    advertisementBytes[20] = power;

    NSData * const advertisementData = [NSData dataWithBytes:advertisementBytes length:sizeof(advertisementBytes)];
    return [NSDictionary dictionaryWithObject:advertisementData forKey:@"kCBAdvDataAppleBeaconKey"];
}
@end


@interface STBeaconAdvertiser : NSObject<CBPeripheralManagerDelegate>
@end
@implementation STBeaconAdvertiser {
    NSDictionary *_advertisementData;
    CBMutableService *_service;
}
- (id)init {
    if ((self = [super init])) {
        {
//            NSUUID * const uuid = [[NSUUID alloc] initWithUUIDString:@"0BBB733E-3E26-43CA-A86F-54C6FF138926"];
            NSUUID * const uuid = [[NSUUID alloc] initWithUUIDString:@"B9407F30-F5F8-466E-AFF9-25556B57FE6D"]; //estimote
//            NSUInteger const adId = 2011109592;
            NSUInteger const adId = 2011075574;
//            NSUInteger const adId = 2011100840;
            uint16_t const major = adId >> 16;
            uint16_t const minor = adId & 0xffff;
            _advertisementData = [STBeaconPeripheralDataHelper peripheralDataWithProximityUUID:uuid major:major minor:minor measuredPower:-58];
        }
        {
            CBUUID * const serviceUUID = [CBUUID UUIDWithString:@"CF865C41-C1E1-4F96-84D5-E62B17DC9DB6"];
            CBMutableService * const service = _service = [[CBMutableService alloc] initWithType:serviceUUID primary:YES];
            CBUUID * const characteristicUUID = [CBUUID UUIDWithString:@"712A1C76-F0B3-40F0-89C9-C1E516BC3C79"];
            CBMutableCharacteristic * const characteristic = [[CBMutableCharacteristic alloc] initWithType:characteristicUUID properties:CBCharacteristicPropertyRead value:[@"blahblahblah" dataUsingEncoding:NSUTF8StringEncoding] permissions:CBAttributePermissionsReadable];
            characteristic.descriptors = @[
                [[CBMutableDescriptor alloc] initWithType:[CBUUID UUIDWithString:CBUUIDCharacteristicUserDescriptionString] value:@"AdID"],
            ];
            service.characteristics = @[
                characteristic,
            ];
        }
    }
    return self;
}
- (void)peripheralManagerDidUpdateState:(CBPeripheralManager *)peripheral {
    NSLog(@"%p -%@: %@", self, NSStringFromSelector(_cmd), peripheral);
    switch (peripheral.state) {
        case CBPeripheralManagerStatePoweredOn: {
            [peripheral addService:_service];
            NSMutableDictionary * const advertisementData = @{
//                CBAdvertisementDataLocalNameKey: @"naïve",
                CBAdvertisementDataServiceUUIDsKey: @[
                    _service.UUID,
                ],
            }.mutableCopy;
            [advertisementData addEntriesFromDictionary:_advertisementData];
            [peripheral startAdvertising:advertisementData];
        } break;
        default:
            break;
    }
}
- (void)peripheralManagerDidStartAdvertising:(CBPeripheralManager *)peripheral error:(NSError *)error {
    NSLog(@"%p -%@: %@, %@", self, NSStringFromSelector(_cmd), peripheral, error);
}
- (void)peripheralManager:(CBPeripheralManager *)peripheral didAddService:(CBService *)service error:(NSError *)error {
    NSLog(@"%p -%@: %@, %@, %@", self, NSStringFromSelector(_cmd), peripheral, service, error);
}
- (void)peripheralManager:(CBPeripheralManager *)peripheral didReceiveReadRequest:(CBATTRequest *)request {
    NSLog(@"%p -%@: %@, %@", self, NSStringFromSelector(_cmd), peripheral, request);
    request.value = [@"blahblahblah" dataUsingEncoding:NSUTF8StringEncoding];
    [peripheral respondToRequest:request withResult:CBATTErrorSuccess];
}
@end


int main(int argc, const char * argv[]) {
    @autoreleasepool {
        STBeaconAdvertiser *a = [[STBeaconAdvertiser alloc] init];
        CBPeripheralManager *m = [[CBPeripheralManager alloc] initWithDelegate:a queue:dispatch_get_main_queue()];
        [[NSRunLoop mainRunLoop] run];
        (void)a;
        (void)m;
    }
    return 0;
}

