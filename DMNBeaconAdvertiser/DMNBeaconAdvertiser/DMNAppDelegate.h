//  Copyright (c) 2014 Fairfax Media. All rights reserved.

#import <UIKit/UIKit.h>


@interface DMNAppDelegate : UIResponder <UIApplicationDelegate>
@end
