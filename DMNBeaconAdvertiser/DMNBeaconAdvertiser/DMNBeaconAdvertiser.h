//  Copyright (c) 2014 Fairfax Media. All rights reserved.

#import <Foundation/Foundation.h>

@import CoreBluetooth;


@interface DMNBeaconAdvertiser : NSObject
- (id)initWithProximityUUID:(NSUUID *)uuid adId:(uint32_t)adId;
@end
