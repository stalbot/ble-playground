//
//  main.m
//  DMNBeaconAdvertiser
//
//  Created by Scott Talbot on 10/04/2014.
//  Copyright (c) 2014 Fairfax Media. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "DMNAppDelegate.h"

int main(int argc, char * argv[])
{
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([DMNAppDelegate class]));
    }
}
