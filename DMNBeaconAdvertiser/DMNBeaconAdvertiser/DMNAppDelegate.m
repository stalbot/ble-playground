//  Copyright (c) 2014 Fairfax Media. All rights reserved.

#import "DMNAppDelegate.h"

@import CoreBluetooth;

#import "DMNBeaconAdvertiser.h"


@interface DMNAppDelegate ()
@end

@implementation DMNAppDelegate {
@private
    DMNBeaconAdvertiser *_beaconAdvertiser;
}

@synthesize window = _window;
- (void)setWindow:(UIWindow *)window {
    _window = window;
    [_window makeKeyAndVisible];
}

- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions {
    UIWindow * const window = [[UIWindow alloc] initWithFrame:[[UIScreen mainScreen] bounds]];
    window.backgroundColor = [UIColor blackColor];

    self.window = window;

    NSUInteger const adId = 2011109592;

    NSUUID * const uuid = [[NSUUID alloc] initWithUUIDString:@"0BBB733E-3E26-43CA-A86F-54C6FF138926"];
//    NSUUID * const uuid = [[NSUUID alloc] initWithUUIDString:@"B9407F30-F5F8-466E-AFF9-25556B57FE6D"]; //estimote

    _beaconAdvertiser = [[DMNBeaconAdvertiser alloc] initWithProximityUUID:uuid adId:adId];

    return YES;
}

@end
