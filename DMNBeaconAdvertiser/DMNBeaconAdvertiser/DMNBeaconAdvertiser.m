//  Copyright (c) 2014 Fairfax Media. All rights reserved.

#import "DMNBeaconAdvertiser.h"

@import CoreLocation;
@import CoreBluetooth;


@interface DMNBeaconAdvertiser () <CBPeripheralManagerDelegate>
@end

@implementation DMNBeaconAdvertiser {
    CBPeripheralManager *_peripheralManager;
    CLBeaconRegion *_beaconRegion;
}

- (id)initWithProximityUUID:(NSUUID *)uuid adId:(uint32_t)adId {
    if ((self = [super init])) {
        _peripheralManager = [[CBPeripheralManager alloc] initWithDelegate:self queue:nil options:nil];
        uint16_t const major = adId >> 16;
        uint16_t const minor = adId & 0xffff;
        _beaconRegion = [[CLBeaconRegion alloc] initWithProximityUUID:uuid major:major minor:minor identifier:uuid.UUIDString];
    }
    return self;
}

- (void)dealloc {
    [_peripheralManager stopAdvertising];
}


#pragma mark - CBPeripheralManagerDelegate

- (void)peripheralManagerDidUpdateState:(CBPeripheralManager *)peripheral {
    NSLog(@"%p -%@: %@", self, NSStringFromSelector(_cmd), peripheral);
    switch (peripheral.state) {
        case CBPeripheralManagerStatePoweredOn: {
            NSMutableDictionary * const advertisementData = @{
                CBAdvertisementDataLocalNameKey: @"Domain",
            }.mutableCopy;
            [advertisementData addEntriesFromDictionary:[_beaconRegion peripheralDataWithMeasuredPower:nil]];
            [peripheral startAdvertising:advertisementData];
        } break;
        default:
            break;
    }
}

- (void)peripheralManagerDidStartAdvertising:(CBPeripheralManager *)peripheral error:(NSError *)error {
    NSLog(@"%p -%@: %@, %@", self, NSStringFromSelector(_cmd), peripheral, error);
}

@end
