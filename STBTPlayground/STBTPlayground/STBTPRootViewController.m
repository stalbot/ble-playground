//  Copyright (c) 2014 Scott Talbot. All rights reserved.

#import "STBTPRootViewController.h"

@import CoreBluetooth;


@interface STBTPRootViewController () <CBCentralManagerDelegate>
@property (nonatomic,strong) IBOutlet UITextView *adIdTextView;
@end

@implementation STBTPRootViewController

+ (instancetype)viewController {
    return [[self alloc] initWithNibName:nil bundle:nil];
}

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil {
    if ((self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil])) {
    }
    return self;
}

- (void)viewDidLoad {
    [super viewDidLoad];
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
}


#pragma mark - STBTPBeaconDelegate

- (void)stbtpDidEnterRegion {
    self.view.backgroundColor = [UIColor greenColor];
}

- (void)stbtpDidExitRegion {
    self.view.backgroundColor = [UIColor redColor];
}

- (void)stbtpDidFindAdID:(NSString *)adID {
}


#pragma mark - CBCentralManagerDelegate

- (void)centralManagerDidUpdateState:(CBCentralManager *)central {
    
}

@end
