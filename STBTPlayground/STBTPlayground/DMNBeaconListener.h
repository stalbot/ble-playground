//  Copyright (c) 2014 Scott Talbot. All rights reserved.

#import <Foundation/Foundation.h>


@class DMNBeaconListener;

@protocol DMNBeaconListenerDelegate <NSObject>
- (void)beaconListener:(DMNBeaconListener *)listener foundBeaconWithAdId:(NSUInteger)adId;
@end


@interface DMNBeaconListener : NSObject
- (id)initWithProximityUUID:(NSUUID *)uuid;
@property (nonatomic,copy,readonly) NSUUID *proximityUUID;
@property (nonatomic,weak) id<DMNBeaconListenerDelegate> delegate;
@end
