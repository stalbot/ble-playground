//  Copyright (c) 2014 Scott Talbot. All rights reserved.

#import "STBTPAppDelegate.h"

@import CoreBluetooth;
@import CoreLocation;

#import "DMNBeaconListener.h"

#import "STBTPRootViewController.h"


static NSString * const STBeaconProximityUUIDString = @"0BBB733E-3E26-43CA-A86F-54C6FF138926";


extern STBTPAppDelegate *STBTPSharedAppDelegate(void) {
    return [[UIApplication sharedApplication] delegate];
}


@interface STBTPAppDelegate () <DMNBeaconListenerDelegate>
@property (nonatomic,strong) DMNBeaconListener *beaconListener;
@end

@implementation STBTPAppDelegate

@synthesize window = _window;
- (void)setWindow:(UIWindow *)window {
    _window = window;
    [_window makeKeyAndVisible];
}

- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions {
    UIWindow * const window = [[UIWindow alloc] initWithFrame:[[UIScreen mainScreen] bounds]];
    window.backgroundColor = [UIColor orangeColor];
    self.window = window;

    STBTPRootViewController * const vc = [STBTPRootViewController viewController];
    self.beaconDelegate = vc;
    window.rootViewController = vc;

    NSUUID * const beaconUUID = [[NSUUID alloc] initWithUUIDString:STBeaconProximityUUIDString];
    _beaconListener = [[DMNBeaconListener alloc] initWithProximityUUID:beaconUUID];
    _beaconListener.delegate = self;

    return YES;
}


#pragma mark - DMNBeaconListenerDelegate

- (void)beaconListener:(DMNBeaconListener *)listener foundBeaconWithAdId:(NSUInteger)adId {
    NSLog(@"%p -%@: %lu", self, NSStringFromSelector(_cmd), (unsigned long)adId);
}

@end
