//  Copyright (c) 2014 Scott Talbot. All rights reserved.

@import UIKit;

#import "STBTPAppDelegate.h"


int main(int argc, char * argv[]) {
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([STBTPAppDelegate class]));
    }
}
