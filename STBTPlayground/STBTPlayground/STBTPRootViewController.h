//  Copyright (c) 2014 Scott Talbot. All rights reserved.

@import UIKit;

#import "STBTPAppDelegate.h"


@interface STBTPRootViewController : UIViewController<STBTPBeaconDelegate>
+ (instancetype)viewController;
@end
