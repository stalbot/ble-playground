//  Copyright (c) 2014 Scott Talbot. All rights reserved.

@import UIKit;


@class STBTPAppDelegate;

extern STBTPAppDelegate *STBTPSharedAppDelegate(void);


@protocol STBTPBeaconDelegate <NSObject>
- (void)stbtpDidEnterRegion;
- (void)stbtpDidExitRegion;
- (void)stbtpDidFindAdID:(NSString *)adID;
@end

@interface STBTPAppDelegate : UIResponder <UIApplicationDelegate>
@property (nonatomic,weak) id<STBTPBeaconDelegate> beaconDelegate;
@end
