//  Copyright (c) 2014 Scott Talbot. All rights reserved.

#import "DMNBeaconListener.h"

@import CoreLocation;
@import CoreBluetooth;


@interface DMNBeaconListener () <CLLocationManagerDelegate>
@end

@implementation DMNBeaconListener {
@private
    CLLocationManager *_locationManager;
    CLBeaconRegion *_beaconRegion;
    NSUInteger _lastNotifiedAdId;
}

- (id)init {
    return [self initWithProximityUUID:nil];
}
- (id)initWithProximityUUID:(NSUUID *)uuid {
    if ((self = [super init])) {
        _proximityUUID = uuid.copy;
        _locationManager = [[CLLocationManager alloc] init];
        _locationManager.delegate = self;

        _beaconRegion = [[CLBeaconRegion alloc] initWithProximityUUID:uuid identifier:uuid.UUIDString];
        _beaconRegion.notifyEntryStateOnDisplay = YES;
        [_locationManager requestStateForRegion:_beaconRegion];
        [_locationManager startMonitoringForRegion:_beaconRegion];
    }
    return self;
}

- (void)dealloc {
    [_locationManager stopRangingBeaconsInRegion:_beaconRegion];
    [_locationManager stopMonitoringForRegion:_beaconRegion];
}


#pragma mark - CLLocationManagerDelegate

- (void)locationManager:(CLLocationManager *)manager didFailWithError:(NSError *)error {
    NSLog(@"%p -%@: %@", self, NSStringFromSelector(_cmd), error);
}

- (void)locationManager:(CLLocationManager *)manager didDetermineState:(CLRegionState)state forRegion:(CLRegion *)region {
    if ([region.identifier isEqualToString:_beaconRegion.identifier]) {
        switch (state) {
            case CLRegionStateInside:
                [manager startRangingBeaconsInRegion:(CLBeaconRegion *)region];
                break;
            case CLRegionStateOutside:
            case CLRegionStateUnknown:
                [manager stopRangingBeaconsInRegion:(CLBeaconRegion *)region];
                break;
        }
    }
}

- (void)locationManager:(CLLocationManager *)manager didRangeBeacons:(NSArray *)beacons inRegion:(CLBeaconRegion *)region {
    id<DMNBeaconListenerDelegate> const delegate = self.delegate;

    [manager stopRangingBeaconsInRegion:region];

    CLBeacon * const closestBeacon = [beacons sortedArrayUsingComparator:^NSComparisonResult(CLBeacon *a, CLBeacon *b) {
        CLProximity const aProximity = a.proximity;
        CLProximity const bProximity = b.proximity;
        if (aProximity != CLProximityUnknown && bProximity != CLProximityUnknown) {
            if (aProximity < bProximity) {
                return NSOrderedAscending;
            }
            if (aProximity > bProximity) {
                return NSOrderedDescending;
            }
        }
        NSInteger const aRSSI = a.rssi;
        NSInteger const bRSSI = b.rssi;
        if (aRSSI > bRSSI) {
            return NSOrderedAscending;
        }
        if (aRSSI < bRSSI) {
            return NSOrderedDescending;
        }

        return NSOrderedSame;
    }].firstObject;

    if (closestBeacon) {
        uint16_t const major = closestBeacon.major.unsignedIntegerValue;
        uint16_t const minor = closestBeacon.minor.unsignedIntegerValue;
        uint32_t const adId = major << 16 | minor;
        if (adId && adId != _lastNotifiedAdId) {
            _lastNotifiedAdId = adId;
            [delegate beaconListener:self foundBeaconWithAdId:adId];
        }
    }
}
- (void)locationManager:(CLLocationManager *)manager rangingBeaconsDidFailForRegion:(CLBeaconRegion *)region withError:(NSError *)error {
    NSLog(@"%p -%@: %@, %@", self, NSStringFromSelector(_cmd), region, error);
}

@end
